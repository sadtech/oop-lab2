package ru.rsreu.oop_lab2.human;

public class Prof extends Person{

    private String fDep;

    public Prof(String fName, String fDep) {
        super(fName);
        this.fDep = fDep;
    }

    public String getfDep() {
        return fDep;
    }

}
