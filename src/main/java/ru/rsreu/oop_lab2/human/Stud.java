package ru.rsreu.oop_lab2.human;

public class Stud extends Person {

    private Integer group;

    public Stud(String fName, int group) {
        super(fName);
        this.group = group;
    }

    public Integer getGroup() {
        return group;
    }
}
