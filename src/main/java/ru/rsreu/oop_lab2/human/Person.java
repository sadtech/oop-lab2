package ru.rsreu.oop_lab2.human;

public abstract class Person {

    protected String fName;

    public Person(String fName) {
        this.fName = fName;
    }

    public String getfName() {
        return fName;
    }

}
