package ru.rsreu.oop_lab2.task3;

public class Task3 {

    private Class aClass;

    public Task3(Class aClass) {
        this.aClass = aClass;
    }

    public String getName() {
        return aClass.getName();
    }

    public String getSuperName() {
        return aClass.getSuperclass().getName();
    }
}
