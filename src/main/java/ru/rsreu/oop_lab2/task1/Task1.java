package ru.rsreu.oop_lab2.task1;

import ru.rsreu.oop_lab2.human.Prof;
import ru.rsreu.oop_lab2.human.Stud;

import java.util.ArrayList;

public class Task1 {

    private ArrayList<Stud> studs;
    private ArrayList<Prof> profs;

    public Task1() {
        studs = new ArrayList<Stud>();
        profs = new ArrayList<Prof>();
    }

    public ArrayList<Stud> getStuds() {
        return studs;
    }

    public ArrayList<Prof> getProfs() {
        return profs;
    }
}
