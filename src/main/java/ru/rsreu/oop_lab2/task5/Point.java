package ru.rsreu.oop_lab2.task5;

public class Point {

    private double x;
    private double y;

    public Point(double x) {
        this.x = x;
        this.y = calculationY(x);
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    private double calculationY(double x) {
        if (x >= 0 && x < 50) {
            return 0;
        } else if (x >= 50 && x < 100) {
            return x - 50;
        } else if (x >= 100 && x < 150) {
            return 50;
        } else if (150 <= x && x < 200) {
            return -(x - 200);
        } else {
            return 0;
        }
    }
}
