package ru.rsreu.oop_lab2.task5;

import java.util.ArrayList;
import java.util.List;

public class Task5 {

    private double to;
    private double from;
    private double h;
    private List<Point> pointArrayList;

    public Task5(double from, double h, double to) {
        this.to = to;
        this.from = from;
        this.h = h;
        pointArrayList = new ArrayList<Point>();
    }

    public ArrayList<Point> getPointArrayList() {
        return (ArrayList) pointArrayList;
    }

    public void calculate() {
        for (double i = from; i < to; i += h) {
            pointArrayList.add(new Point(i));
        }
    }
}
