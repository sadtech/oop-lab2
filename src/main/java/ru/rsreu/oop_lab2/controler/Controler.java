package ru.rsreu.oop_lab2.controler;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.chart.StackedAreaChart;
import javafx.scene.chart.XYChart;
import javafx.scene.control.*;
import ru.rsreu.oop_lab2.human.Prof;
import ru.rsreu.oop_lab2.human.Stud;
import ru.rsreu.oop_lab2.task1.Task1;
import ru.rsreu.oop_lab2.task3.Task3;
import ru.rsreu.oop_lab2.task5.Point;
import ru.rsreu.oop_lab2.task5.Task5;

import java.net.URL;
import java.util.ResourceBundle;

public class Controler implements Initializable {

    @FXML
    private StackedAreaChart<Number, Number> test;

    @FXML
    private TextField task1_field_group, task1_field_fname, task5_field_from, task5_field_h, task5_field_to;

    @FXML
    private RadioButton task1_radio_prepod, task1_radio_stud;

    @FXML
    private TextArea task1_textarea_print;

    @FXML
    private Label task1_label_group;

    private Task1 task1;

    public void initialize(URL location, ResourceBundle resources) {
        task1 = new Task1();
        task1_radio_stud.setSelected(true);
    }

    public void task5_print() {
        double from = Double.parseDouble(task5_field_from.getText());
        double h = Double.parseDouble(task5_field_h.getText());
        double to = Double.parseDouble(task5_field_to.getText());
        Task5 task5 = new Task5(from, h, to);
        task5.calculate();
        XYChart.Series series = new XYChart.Series();
        for (Point point : task5.getPointArrayList()) {
            series.getData().add(new XYChart.Data(point.getX(), point.getY()));
        }
        test.getData().add(series);
    }

    public void task1_set_radioStud() {
        task1_radio_prepod.setSelected(false);
        task1_label_group.setText("Группа");
    }

    public void task1_set_radioPrepod() {
        task1_radio_stud.setSelected(false);
        task1_label_group.setText("Кафедра");
    }

    public void task1_addPerson() {
        if (!task1_field_fname.getText().isEmpty() && !task1_field_group.getText().isEmpty()) {
            if (task1_radio_stud.isSelected()) {
                task1.getStuds().add(new Stud(task1_field_fname.getText(), Integer.parseInt(task1_field_group.getText())));
            } else if (task1_radio_prepod.isSelected()) {
                task1.getProfs().add(new Prof(task1_field_fname.getText(), task1_field_group.getText()));
            }
            alert("Запись добавлена!");
        } else {
            alert("Запись не добавлена!");
        }
    }

    public void task1_printPerson() {
        task1_textarea_print.clear();
        if (task1_radio_stud.isSelected()) {
            for (Stud stud : task1.getStuds()) {
                task1_textarea_print.appendText("Фамилия: " + stud.getfName() + "  / Группа: " + stud.getGroup() + "\n");
            }
        } else if (task1_radio_prepod.isSelected()) {
            for (Prof prof : task1.getProfs()) {
                task1_textarea_print.appendText("Фамилия: " + prof.getfName() + "  / Кафедра: " + prof.getfDep() + "\n");
            }
        } else {
            task1_textarea_print.clear();
        }
    }

    public void task2_run() {
        alert("Имя класса: " + Controler.class.getName() + "\nПредок: " + Controler.class.getSuperclass().getName());
    }

    public void task3_run() {
        Task3 task3 = new Task3(Task1.class);
        alert("Имя класса: " + task3.getName() + "\nПредок: " + task3.getSuperName());
    }

    public void task4_PrintInfoStud() {
        Task3 task3 = new Task3(Stud.class);
        alert("Имя класса: " + task3.getName() + "\nПредок: " + task3.getSuperName());
    }

    public void task4_PrintInfoProf() {
        Task3 task3 = new Task3(Prof.class);
        alert("Имя класса: " + task3.getName() + "\nПредок: " + task3.getSuperName());
    }

    private void alert(String text) {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Information");
        alert.setHeaderText(null);
        alert.setContentText(text);
        alert.showAndWait();
    }

}
